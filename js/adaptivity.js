$(function() {
    /*calculate browser width*/
    var browser_width=window.outerWidth-10;

    if (browser_width < 1024) {
       $( ".inner_main" ).replaceWith('<div class="inner_main"><div class="fingers_captures intelligence"><a href="http://uni5.org/index.php/intelligence" class="text">Intelligence - Education</a>                    <a href="http://uni5.org/index.php/intelligence"><img src="img/fire.png" alt="Intelligence - Education" class="icons"></a></div><div class="fingers_captures awareness"><a href="http://uni5.org/index.php/awareness" class="text"><p>Self Awareness -</p><p>Selftual</p></a><a href="http://uni5.org/index.php/awareness"><img src="img/air.png" alt="Self Awareness - Selftual" class="icons"></a></div><div class="fingers_captures mind"><a href="http://uni5.org/index.php/second" class="text"><p>Mind -</p><p>Harmony</p></a><a href="http://uni5.org/index.php/second"><img src="img/water.png" alt="Mind - Harmony" class="icons"></a></div><div class="fingers_captures consciousness"><a href="http://uni5.org/index.php/conscious" class="text"><p>Consciousness -</p><p>Service</p></a><a href="http://uni5.org/index.php/conscious"><img src="img/space.png" alt="Consciousness - Service" class="icons"></a></div><div class="fingers_captures body"><a href="http://uni5.org/index.php/first" class="text"><p>Body -</p><p>Health</p></a><a href="http://uni5.org/index.php/first"><img src="img/earth.png" alt="Body - Health" class="icons"></a></div></div>');
       $('.fingers_captures').find('a').css('text-align','center');

       $('.awareness').find('a').css('display','block');
       $('.awareness').find('a').css('padding','0px 20px');
       $('.awareness').find('p').css('margin-bottom','0');
       $('.awareness').css('position','relative');
       $('.awareness').css('left','49px');
       $('.awareness').css('bottom','91px');
       $('.awareness').find('img').css('margin-top','7px');

       $('.consciousness').find('a').css('display','block');
       $('.consciousness').find('a').css('padding','0px 20px');
       $('.consciousness').find('p').css('margin-bottom','0');
       $('.consciousness').css('position','relative');
       $('.consciousness').css('left','-13px');
       $('.consciousness').css('bottom','96px');
       $('.consciousness').find('img').css('margin-top','7px');

       $('.intelligence').css('position','relative');
       $('.intelligence').css('left','20px');
       $('.intelligence').css('bottom','5px');
       $('.intelligence').css('margin-bottom','41px');

       $('.mind').find('a').css('display','block');
       $('.mind').find('a').css('padding','0px 20px');
       $('.mind').find('p').css('margin-bottom','0');
       $('.mind').css('position','relative');
       $('.mind').css('left','-64px');
       $('.mind').css('bottom','94px');
       $('.mind').find('img').css('margin-top','7px');

       $('.body').find('a').css('display','block');
       $('.body').find('a').css('padding','0px 20px');
       $('.body').find('p').css('margin-bottom','0');
       $('.body').css('position','relative');
       $('.body').css('left','5px');
       $('.body').css('bottom','152px');
       $('.body').find('img').css('margin-top','7px');

       $('.inner_main').css('margin-bottom','142px');
       $('.inner_main').css('margin-left','0');
       $('.inner_main').css('margin','auto');
       $('.inner_main').css('margin-bottom','170px');
       $('.inner_main').css('width','550px');

       $('.all_the_media').css('position','static');
       $('.all_the_media').css('font-size','18px');
       $('.all_the_media').css('width','526px');
       $('.all_the_media').css('margin-bottom','10px');
       
       $('.hand').css('bottom','534px');
    }
    if (browser_width < 560) {
        $( ".inner_main" ).replaceWith('<div class="inner_main"><div class="fingers_captures intelligence"><a href="http://uni5.org/index.php/intelligence" class="text"><p>Intelligence -</p><p>Education</p></a><a href="http://uni5.org/index.php/intelligence"><img src="img/fire.png" alt="Intelligence - Education" class="icons"></a></div><div class="fingers_captures awareness"><a href="http://uni5.org/index.php/awareness" class="text"><p>Self</p><p>Awareness -</p><p>Selftual</p></a><a href="http://uni5.org/index.php/awareness"><img src="img/air.png" alt="Self Awareness - Selftual" class="icons"></a></div><div class="fingers_captures mind"><a href="http://uni5.org/index.php/second" class="text"><p>Mind -</p><p>Harmony</p></a><a href="http://uni5.org/index.php/second"><img src="img/water.png" alt="Mind - Harmony" class="icons"></a></div><div class="fingers_captures consciousness"><a href="http://uni5.org/index.php/conscious" class="text"><p>Conscio-</p><p>usness -</p><p>Service</p></a><a href="http://uni5.org/index.php/conscious"><img src="img/space.png" alt="Consciousness - Service" class="icons"></a></div><div class="fingers_captures body"><a href="http://uni5.org/index.php/first" class="text"><p>Body -</p><p>Health</p></a><a href="http://uni5.org/index.php/first"><img src="img/earth.png" alt="Body - Health" class="icons"></a></div></div>');
        $('body').css('font-weight','500');
        $('#cssmenu').css('font-weight','900');

        $('main').css('min-width','100%');
        $('main').css('display','flex');
        $('main').css('flex-wrap','wrap');
        $('main').css('padding-top','14px');

        $('.all_the_media').css('width','293px');
        $('.all_the_media').css('order','-1');
        $('.all_the_media').css('position','static');
        $('.all_the_media').css('font-size','18px');
        $('.all_the_media').css('margin-bottom','125px');

        $('.hand_link').css('width','100%');
        $('.hand').css('bottom','570px');
        $('.hand').css('left','-1px');
        $('.hand').css('width','250px');

        $('.fingers_captures').find('a').css('text-align','center');
        $('.fingers_captures').find('a').css('font-size','18px');
        $('.fingers_captures').find('a').css('line-height','24px');
        $('.fingers_captures').find('img').css('width','42px');

        $('.awareness').find('a').css('display','block');
        $('.awareness').find('a').css('padding','0px 20px');
        $('.awareness').find('p').css('margin-bottom','0');
        $('.awareness').css('position','relative');
        $('.awareness').css('left','-15px');
        $('.awareness').css('bottom','279px');
        $('.awareness').find('img').css('margin-top','7px');
        $('.awareness').find('img').css('position','relative');
        $('.awareness').find('img').css('top','10px');
        $('.awareness').find('img').css('left','31px');

        $('.consciousness').find('a').css('display','block');
        $('.consciousness').find('a').css('padding','0px 20px');
        $('.consciousness').find('p').css('margin-bottom','0');
        $('.consciousness').css('position','relative');
        $('.consciousness').css('left','-19px');
        $('.consciousness').css('bottom','281px');
        $('.consciousness').find('img').css('margin-top','7px');
        $('.consciousness').find('img').css('position','relative');
        $('.consciousness').find('img').css('bottom','2px');

        $('.intelligence').css('position','relative');
        $('.intelligence').css('left','13px');
        $('.intelligence').css('bottom','97px');
        $('.intelligence').css('margin-bottom','103px');
        $('.intelligence').find('p').css('margin-bottom','0');
        $('.intelligence').find('img').css('position','relative');
        $('.intelligence').find('img').css('right','16px');
        $('.intelligence').find('img').css('top','12px');

        $('.mind').find('a').css('display','block');
        $('.mind').find('a').css('padding','0px 20px');
        $('.mind').find('a').css('position','relative');
        $('.mind').find('a').css('left','24px');
        $('.mind').find('a').css('bottom','14px');
        $('.mind').find('p').css('margin-bottom','0');
        $('.mind').css('position','relative');
        $('.mind').css('left','-25px');
        $('.mind').css('bottom','249px');
        $('.mind').find('img').css('margin-top','7px');
        $('.mind').find('img').css('position','relative');
        $('.mind').find('img').css('top','13px');
        $('.mind').find('img').css('right','29px');

        $('.body').find('a').css('display','block');
        $('.body').find('a').css('padding','0px 20px');
        $('.body').find('p').css('margin-bottom','0');
        $('.body').css('position','relative');
        $('.body').css('left','18px');
        $('.body').css('bottom','298px');
        $('.body').find('img').css('margin-top','7px');
        $('.body').find('img').css('position','relative');
        $('.body').find('img').css('bottom','2px');

        $('.inner_main').css('width','100%');
        $('.inner_main').css('margin-left','0');
        $('.inner_main').css('margin','auto');
        $('.inner_main').css('margin-bottom','170px');
        $('.inner_main').css('width','320px');
    }
});


